main.factory('structureFactory', function () {
    var  obj  = {
        fields: [
            {
                name: 'action1',
                buttons: [
                    {
                        action: 'action2',
                    },
                     {
                        action: 'action3',
                    },
                     
                ]
            },
           {
                name: 'action2',
                buttons: [
                    {
                        action: 'action1',
                    }
                ]
            },
            {
                name: 'action3',
                buttons: [
                    {
                        action: 'action2',
                    }
                ]
            },
        ]
    };
    return obj;
});