main.factory('someFactory', ['structureFactory', function(structureFactory) {
    var screen = new function () {
        var self = this;
        this.currentElement = structureFactory.fields[0];
       
        this.someClickAction = function(button){
        console.log('You clicked factory function!');
         structureFactory.fields.forEach(function (screen) {
                if (screen.name == button.action) {
                    self.currentElement = screen;
                }
            });
        };
    };

    return screen;
}]);