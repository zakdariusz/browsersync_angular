var main = angular.module('someApp', []);
main.config(function($httpProvider){
    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
});

main.controller('MainCtrl', ['someFactory' , function (someFactory) {
    this.someFactory = someFactory;
}]);